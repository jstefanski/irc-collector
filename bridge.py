# python3
from collections import namedtuple

IRCParams = namedtuple('IRCParams', ['host', 'port', 'user', 'nick', 'password', 'ssl', 'channels'])

def parse_irc_params(irc_url):
    from urllib.parse import urlparse, parse_qs

    url_params = urlparse(irc_url)
    query_params = parse_qs(url_params.query)

    # validate params
    if not url_params.username:
        print("No username provided in IRC url: {0}".format(irc_url))
        return None

    use_ssl = url_params.scheme == "ircs"
    default_port = 6667 if not use_ssl else 6697

    channels = query_params.get('join', [])

    return IRCParams(
        host=url_params.hostname,
        port=url_params.port or default_port,
        nick=url_params.username,
        user=url_params.username,
        password=url_params.password,
        ssl=use_ssl,
        channels=channels,
    )


def run(irc_url, ws_url):
    irc_params = parse_irc_params(irc_url)

    import websockets
    import pydle
    import json
    class Bridge(pydle.Client):
        async def on_connect(self):
            self.ws = await websockets.connect(ws_url)
            await self.ws.send("Connected to IRC")
            for channel in irc_params.channels:
                print('Joining: #' + channel)
                await self.join('#' + channel)

        async def on_join(self, channel, user):
            if user == self.nickname:
                await self.message(channel, 'bot online!')

        async def on_raw_004(self, message):
            print('004', message)

        async def on_message(self, target, source, message):
            msg = {
                'type': 'irc',
                'irc_command': 'PRIVMSG',
                'irc_target': target,
                'irc_source': source,
                'irc_message': message,
            }
            await self.ws.send(json.dumps(msg))

    bridge = Bridge(
        nickname=irc_params.nick,
        realname='irc-websocket-bridge',
    )
    bridge.run(
        hostname=irc_params.host,
        port=irc_params.port,
        tls=irc_params.ssl,
        tls_verify=False,
        password=irc_params.password,
    )


if __name__ == '__main__':
    from argparse import ArgumentParser
    import os

    # TODO: Perhaps this should be a multi-server, multi-channel bridge
    # in that we can specify one or more IRC and WS URLs to connect to
    # especially with using the path of the IRC URL to determine channel

    parser = ArgumentParser()
    parser.add_argument('--irc_url',
        default=os.getenv('IRC_URL'),
        help="URL for IRC server, in the form: irc://nickname:password@host:port/?join=channel1&join=channel2")
    parser.add_argument("--ws_url",
        default=os.getenv('WS_URL'),
        help="URL for Websocket server, in the form: ws://host:port")

    args = parser.parse_args()
    run(args.irc_url, args.ws_url)