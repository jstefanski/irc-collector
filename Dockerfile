FROM python:latest

COPY requirements.txt /tmp/requirements.txt
RUN python -m pip install -r /tmp/requirements.txt

WORKDIR /app
COPY bridge.py bridge.py

ENV IRC_URL=
ENV WS_URL=

ENTRYPOINT ["sh", "-c", "python /app/bridge.py ${IRC_URL} ${WS_URL}"]